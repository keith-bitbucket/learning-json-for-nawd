<?php

$rush = array();

$member = new stdClass();

$member->name = "Geddy Lee";
$member->hair = "long";
$rush[] = $member;

$member->name = "Alex Lifeson";
$member->hair = "short";
$rush[] = $member;

$member->name = "Neil Peart";
$member->hair = "shorter";
$rush[] = $member;

$json = json_encode($rush);

?>

<script type="text/javascript">
var obj = JSON.parse('<?=$json?>');
console.log(obj);
</script>
