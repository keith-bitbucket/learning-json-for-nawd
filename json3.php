<pre>

<?php

echo "\n\n\n=========================== OBJECT:\n\n";
$rush = array();

$member = new stdClass();

$member->name = "Geddy Lee";
$member->hair = "long";
$rush[] = $member;

$member->name = "Alex Lifeson";
$member->hair = "short";
$rush[] = $member;

$member->name = "Neil Peart";
$member->hair = "shorter";
$rush[] = $member;

print_r($rush);


echo "\n\n\n=========================== JSON ENCODE\n\n";
$json = json_encode($rush);
echo $json;


echo "\n\n\n=========================== JSON DECODE\n";
$band = json_decode($json);
print_r($band);


echo "\n\n\n=========================== JSON DECODE (as ASSOC)\n";
$band = json_decode($json, true);
print_r($band);


